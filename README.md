# TODO burn-up

Tired of tickets-tracking systems? TODO burn-up helps you add a bit of project
management metrics into your "à l'arrache" project with a minimum effort.

TODO burn-up provides a simple way to extract metrics from git history and a simple TODO file.

This TODO file is just a list of tasks:
```
  + 8 done tasks begin with '+' ('8' is the estimation)
  / 2 current tasks begin with '/'
  - 4 todo tasks begin with '-'
```

Features:
  - evaluates the workdays from each author browsing commits
  - draws a burn-up, with forecast to reach the desired milestone
  - draws code quality metrics

Example resulting burn-up:

![Example burn-up](samples/burnup.png)

Example of code metrics:

![Code metrics](samples/comments_tests.png)

![Code volume](samples/volume.png)

![TODOs](samples/todos.png)

## TODO file Syntax


A task is a line starting with '-', '/' or '+'. This first character means:
 - `+` = done task
 - `-` = todo task
 - `/` = in progress

After this character, follows the estimation of the task (in points, days, hours or whatever you want.)
Finally the rest of the line is what you want (probably a description of the task.)

A Milestone is one line beginning with `#` and followed by one or more tasks belonging to it.

The file can also contain comments (lines beginning with `#` not followed by tasks). Empty lines are ignored.

```markdown
# Milestone 1
+ 2 Done task
/ 1 Current task
- 2 Next task

# Milestone 2
- 10 Another task
- 16 Yet another big task
```

## Setup

```console
$ python3.11 -m venv venv
$ source venv/bin/activate
(venv) $ pip install .
```

## Basic usage

```python
from burnup import ProjectConfiguration, PYTHON_CONFIGURATION, draw_all

config = ProjectConfiguration(
        title="My great project",
        code_paths="src",
        tests_paths="tests",
        language=PYTHON_CONFIGURATION,
        effort_forcast=5/7,  # ie. one developper will work full time 5 days per week in the next months
    )
draw_all(config)
```

Look at ProjectConfiguration documentation for advanced usage.