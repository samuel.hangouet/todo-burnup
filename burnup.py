#!/usr/bin/env python3
"""
Extracts code and progress metrics history to draw project management diagrams.
"""
from typing import Callable
from pathlib import Path
import sys
import re
from dataclasses import dataclass, field
from datetime import datetime, timedelta
from subprocess import check_output, check_call, CalledProcessError

import matplotlib.pyplot as plt
from tqdm import tqdm


@dataclass(kw_only=True)
class LanguageConfiguration:
    code_suffixes: tuple[str, ...]
    "Suffixes of a file to be considered as a code file."

    comment_pattern: str
    "A regular expression to count line comments"

    todo_in_comment_pattern: str
    "A regular expression to count TODO comments"

    not_a_line_of_code_patterns: tuple[str, ...] = (r"^\s*$",)
    "A tuple of regular expressions to exclude lines from effective line of codes."


CPP_CONFIGURATION = LanguageConfiguration(
    code_suffixes=(".cpp", ".h", ".hpp"),
    comment_pattern=r"// ",
    todo_in_comment_pattern=r"// *TODO",
    not_a_line_of_code_patterns=(
        r"^\s*#",
        r"^\s*$",
        r"^\s*//",
        r"^\s*[\[{()}\],]\s*$",
    ),
)

PYTHON_CONFIGURATION = LanguageConfiguration(
    code_suffixes=(".py",),
    comment_pattern=r"# ",
    todo_in_comment_pattern=r"#\s*TODO",
    not_a_line_of_code_patterns=(r"^\s*#", r"^\s*$"),
)


@dataclass(kw_only=True)
class ProjectConfiguration:
    title: str
    "Project title"

    code_paths: str | tuple[str, ...]
    "Where to look for code files. The first existing directory will be used."

    tests_paths: str | tuple[str, ...]
    "Where to look for tests. The first existing directory will be used."

    language: LanguageConfiguration
    "Language specific settings"

    effort_forecast: float
    "Factor to convert calendar days into worked days."

    shown_milestones: int = -1
    "Number of milestones to keep in the burn-up. -1 means all."

    todo_backlog_paths: str | tuple[str, ...] = ("todo", "TODO", "README.md")
    "The path/name of the TODO file."

    ignore_errors_in_todo: bool = True
    "Whether we tolerate syntax errors in the TODO file or absence of file."

    milestone_line_pattern: str = r"# +(.*)"
    "A regex to extract milestone names from the TODO file."

    comment_prefix: str = "#"
    "Comments mark (milestones can be extracted from comments)."

    counted_suffixes: tuple[str, ...] = ()
    "The file kinds to count in metrics."

    dayoff_threshold: int = 7
    "Number of consecutive days without a commit to consider an author as off."

    burnup_step_in_days: int = 2
    "Burn-up curves resolution (for project with a lot of commits, consider increasing this not to wait too much.)"

    ignore_first_commits: int = 0
    "Eventual initial commits to ignore (e.g. to get rid of outliers in metrics, or not yet existing TODO file)"


@dataclass(kw_only=True)
class Progress:
    done: int = 0
    todo: int = 0

    @property
    def total(self):
        return self.done + self.todo


@dataclass(kw_only=True)
class Milestone:
    name: str
    progress: Progress = field(default_factory=Progress)


@dataclass(kw_only=True)
class CodeMetrics:
    lines_of_code: int = 0
    lines_of_test: int = 0
    lines_of_comments: int = 0
    files_by_extension: dict[str, int] = field(default_factory=dict)
    todos: int = 0

    @property
    def total_lines(self):
        return self.lines_of_comments + self.lines_of_test + self.lines_of_code

    @property
    def comment_ratio(self):
        if self.lines_of_comments == 0:
            return 0
        return self.lines_of_comments / (self.lines_of_comments + self.lines_of_code)

    @property
    def test_ratio(self):
        if self.lines_of_test == 0:
            return 0
        return self.lines_of_test / (self.lines_of_test + self.lines_of_code)


@dataclass(kw_only=True)
class Commit:
    git_hash: str
    author_email: str
    author_date: datetime
    committer_date: datetime


@dataclass(kw_only=True)
class ProjectInfo:
    milestones: list[Milestone]
    code_metrics: CodeMetrics


def resolve_path(paths: str | tuple[str, ...], filter: Callable = Path.exists) -> Path:
    """Returns the first found path among the given one(s)."""
    path: Path
    if type(paths) is str:
        paths = (paths,)
    for p in paths:
        path = Path(p)
        if filter(path):
            return path

    raise OSError(f"Cannot find an existing path among {paths}")


def compute_code_metrics(config: ProjectConfiguration) -> CodeMetrics:
    """Computes code quality metrics."""
    code_path = resolve_path(config.code_paths, Path.is_dir)
    tests_path = resolve_path(config.tests_paths, Path.is_dir)

    filter = " -o ".join(f'-name "*{s}"' for s in config.language.code_suffixes)
    code_files = check_output(f"find {code_path} {filter}", shell=True).decode("utf-8")
    code_files = " ".join(code_files.split())
    tests_files = check_output(f"find {tests_path} {filter}", shell=True).decode(
        "utf-8"
    )
    tests_files = " ".join(tests_files.split())
    if code_files:
        comments = int(
            check_output(
                f'grep -e "{config.language.comment_pattern}" {code_files} | wc -l',
                shell=True,
            )
        )
        todos = int(
            check_output(
                f'grep -e "{config.language.todo_in_comment_pattern}" {code_files} | wc -l',
                shell=True,
            )
        )
        code_lines = int(
            check_output(
                "grep -v"
                + "".join(
                    ' -e  "{p}"' for p in config.language.not_a_line_of_code_patterns
                )
                + " "
                + code_files
                + " | wc -l",
                shell=True,
            )
        )
        comments -= todos
    else:
        comments = 0
        todos = 0
        code_lines = 0

    if tests_files:
        tests_lines = int(
            check_output(
                "grep -v"
                + "".join(
                    ' -e  "{p}"' for p in config.language.not_a_line_of_code_patterns
                )
                + " "
                + tests_files
                + " | wc -l",
                shell=True,
            )
        )
    else:
        tests_lines = 0

    files = {}
    for suffix in config.counted_suffixes:
        files[suffix] = int(
            check_output(f"find . -name '*{suffix}' | wc -l", shell=True)
        )

    return CodeMetrics(
        lines_of_code=code_lines,
        lines_of_test=tests_lines,
        lines_of_comments=comments,
        files_by_extension=files,
        todos=todos,
    )


def parse_todo_file(config: ProjectConfiguration) -> list[Milestone]:
    """Extracts milestones progress information from TODO backlog."""
    todo_path = resolve_path(config.todo_backlog_paths)

    # print(f"Parse {todo_path}")

    with open(todo_path, "r") as f:
        lines = f.readlines()

    milestones = []
    current_milestone = Milestone(name="Default")
    milestones.append(current_milestone)
    milestone_re = re.compile(config.milestone_line_pattern)

    for ln, line in enumerate(lines):
        line = line.strip()
        if len(line) == 0:
            continue

        milestone_match = milestone_re.match(line)
        if milestone_match:
            current_milestone = Milestone(name=milestone_match.group(1))
            # Ignore empty milestones
            if len(milestones) > 0 and milestones[-1].progress.total == 0:
                milestones.pop()
            milestones.append(current_milestone)
            continue

        if line.startswith(config.comment_prefix):
            # Comment
            continue

        try:
            status, rest = line.split(" ", 1)
        except ValueError as exc:
            if config.ignore_errors_in_todo:
                continue
            raise ValueError(f"Invalid format line {ln+1}: {line}") from exc

        if status not in ("+", "-", "/"):
            if config.ignore_errors_in_todo:
                continue
            raise ValueError("Invalid first character line {ln+1}:" + status)

        try:
            time, msg = rest.split(" ", 1)
            time = int(time)
        except ValueError as exc:
            if config.ignore_errors_in_todo:
                # Can be a subtask
                continue
            raise ValueError(f"Invalid format line {ln+1}: {line}") from exc

        # Accumulate todo and done times
        assert time <= 256
        if status == "+":
            current_milestone.progress.done += time
        else:
            current_milestone.progress.todo += time

    return milestones


def gather_commits(config: ProjectConfiguration) -> list[Commit]:
    """Parses git log."""
    print("Gather commits from git history")
    lines = (
        check_output(f'git log --pretty=format:"%h;%ae;%aI;%cI"', shell=True)
        .decode()
        .splitlines()
    )
    commits: list[Commit] = []
    for commit_desc in tqdm(reversed(lines), total=len(lines)):
        commit_hash, author_email, author_date, committer_date = commit_desc.split(";")
        author_date = datetime.fromisoformat(author_date)
        committer_date = datetime.fromisoformat(committer_date)
        commits.append(
            Commit(
                git_hash=commit_hash,
                author_date=author_date,
                author_email=author_email,
                committer_date=committer_date,
            )
        )
    commits.sort(key=lambda c: c.committer_date)
    return commits[config.ignore_first_commits :]


def extract_workdays(
    config: ProjectConfiguration, commits: list[Commit]
) -> list[tuple[datetime, int]]:
    """Lists the work days cumulated for each author, according to the days-off threshold."""
    # print("Compute work days")
    start_date: datetime | None = None
    end_date: datetime | None = None
    commits_per_email: dict[str, list[datetime]] = {}
    for commit in tqdm(commits):
        commits_per_email.setdefault(commit.author_email, []).append(commit.author_date)
        start_date = (
            min(start_date, commit.author_date)
            if start_date is not None
            else commit.author_date
        )
        end_date = (
            max(end_date, commit.author_date)
            if end_date is not None
            else commit.author_date
        )

    if end_date is None or start_date is None:
        return []

    workdays = [0] * (1 + (end_date - start_date).days)
    for author_dates in commits_per_email.values():
        author_dates.sort()
        previous_day = None
        for date in author_dates:
            day = (date - start_date).days
            if previous_day is None:
                workdays[day] += 1
            elif day - previous_day <= config.dayoff_threshold:
                for d in range(previous_day + 1, day + 1):
                    workdays[d] += 1
            previous_day = day

    return [(start_date + timedelta(days=d), w) for d, w in enumerate(workdays)]


def extract_project_info(
    config: ProjectConfiguration, commits: list[Commit]
) -> list[tuple[datetime, ProjectInfo]]:
    """Checkouts old commits to extract project information from the past."""
    print("Look at previous versions")
    # Check no local change
    try:
        check_call("git diff --quiet", shell=True)
    except CalledProcessError:
        print("Please, commit your local changes to allow burn-up drawing")
        raise

    branch = check_output("git br --show-current", shell=True).strip().decode()
    last_date: datetime | None = None
    infos = []

    try:
        for c, commit in tqdm(enumerate(commits), total=len(commits)):
            # Check step to avoid analyzing too much commits
            if last_date is not None and c != len(commits) - 1:
                elapsed_days = (commit.committer_date - last_date).days
                if elapsed_days < config.burnup_step_in_days:
                    continue
            last_date = commit.committer_date

            # print(f"Checkout commit {commit.git_hash}")
            check_call("git checkout -f -q " + commit.git_hash, shell=True)

            # Progress
            try:
                milestones = parse_todo_file(config)
                # print(f"Milestones at commit {commit.git_hash}: {milestones}")
            except Exception as e:
                # print(f"Cannot parse TODO file at commit {commit.git_hash}: {e}")
                if config.ignore_errors_in_todo:
                    continue
                raise

            # Code metrics
            code_metrics = compute_code_metrics(config)

            infos.append(
                (
                    commit.committer_date,
                    ProjectInfo(milestones=milestones, code_metrics=code_metrics),
                )
            )

    finally:
        check_call(f"git checkout -f -q {branch}", shell=True)

    return infos


def draw_burn_up(
    config: ProjectConfiguration,
    commits: list[Commit],
    project_info: list[tuple[datetime, ProjectInfo]],
):
    """Draws the burn-up curves."""

    # Burnup curves
    @dataclass
    class Curve:
        label: str = ""
        dates: list[datetime] = field(default_factory=list)
        points: list[int] = field(default_factory=list)

    milestone_curves: list[Curve] = []
    done_curve: Curve = Curve(label="Done")
    total_done = total = 0
    date = datetime.now()
    last_milestone = ""
    for date, info in project_info:
        total_done = 0
        total = 0
        for m, milestone in enumerate(info.milestones):
            if config.shown_milestones != -1 and m >= config.shown_milestones:
                continue
            last_milestone = milestone.name
            total_done += milestone.progress.done
            total += milestone.progress.total
            while m >= len(milestone_curves):
                milestone_curves.append(Curve())
            curve = milestone_curves[m]
            curve.label = milestone.name
            curve.dates.append(date)
            curve.points.append(total)

        done_curve.dates.append(date)
        done_curve.points.append(total_done)

    # Forecast
    if total_done == 0:
        print("No task done, cannot forecast")
        sys.exit(1)
    dates_workdays = extract_workdays(config, commits)
    done_workdays = sum(workday for _, workday in dates_workdays)
    velocity = total_done / done_workdays
    todo_points = total - total_done
    todo_workdays = todo_points / velocity
    todo_days = todo_workdays / config.effort_forecast
    finish_date = date + timedelta(days=todo_days)
    print(f"Total done points={total_done}")
    print(f"Total worked days={done_workdays}")
    print(f"Forecast until milestone '{last_milestone}':")
    print(f"Remaining points={todo_points}")
    print(f"Remaining workdays={todo_workdays}")
    print(f"Velocity={velocity} points per workday")
    print(f"Finish date={finish_date}")

    # Effort
    fig, axe1 = plt.subplots()
    plt.suptitle(config.title, fontsize=14)
    plt.title(f"Velocity: {velocity:.02f} points per workday", fontsize=8)
    effort_curve = Curve(label="Work days")
    workdays = 0
    for date, workday in dates_workdays:
        workdays += workday
        effort_curve.dates.append(date)
        effort_curve.points.append(workdays)

    axe2 = axe1.twinx()
    axe2.plot_date(
        effort_curve.dates,
        effort_curve.points,
        "-",
        label=effort_curve.label,
        color="grey",
    )
    finish_workdays = done_workdays + todo_workdays
    axe2.plot_date(
        [date, finish_date],
        [done_workdays, finish_workdays],
        "--",
        color="grey",
    )
    axe2.set_ylabel("Cumulative working days")
    plt.ylim(top=finish_workdays * 1.2)

    # Burnup
    for curve in milestone_curves:
        curve.dates.append(finish_date)
        curve.points.append(curve.points[-1])
        axe1.plot_date(curve.dates, curve.points, "-", label=curve.label)  # type: ignore
    axe1.plot_date(done_curve.dates, done_curve.points, "--", label=done_curve.label, color="green")  # type: ignore
    axe1.plot_date(
        [date, finish_date],  # type: ignore
        [total_done, total],
        "--",
        label="Forecast",
        color="red",
    )
    axe1.set_ylabel("Points")

    fig.autofmt_xdate()
    fig.legend(loc=2)
    plt.show()


def draw_code_metrics(
    commits: list[Commit], project_info: list[tuple[datetime, ProjectInfo]]
):
    """Draws code quality metrics."""
    dates = []
    files: dict[str, list[int]] = {}
    code_lines = []
    comment_ratio = []
    test_ratio = []
    todos = []

    for date, info in project_info:
        dates.append(date)

        metrics = info.code_metrics
        for suffix, count in metrics.files_by_extension.items():
            files.setdefault(suffix, []).append(count)
        code_lines.append(metrics.total_lines)
        comment_ratio.append(metrics.comment_ratio)
        test_ratio.append(metrics.test_ratio)
        todos.append(metrics.todos / metrics.total_lines)

    fig = plt.figure()
    fig.suptitle("Code metrics")
    plt.plot_date(dates, comment_ratio, "-", label="comments/code ratio")
    plt.plot_date(dates, test_ratio, "-", label="test/code ratio")
    fig.autofmt_xdate()
    fig.legend()
    plt.show()

    fig = plt.figure()
    fig.suptitle("TODOs")
    plt.plot_date(dates, todos, "-", label="todos/line")
    fig.autofmt_xdate()
    fig.legend()
    plt.show()

    fig, ax1 = plt.subplots()
    fig.suptitle("Volume of code")
    ax1.set_ylabel("Lines")
    ax1.plot_date(dates, code_lines, "--", label="code lines")
    ax2 = ax1.twinx()
    for suffix, counts in files.items():
        ax2.plot_date(dates, counts, "-", label=f"{suffix} files")
    ax2.set_ylabel("Files")
    fig.autofmt_xdate()
    fig.legend(loc=2)
    plt.show()


def draw_all(config: ProjectConfiguration):
    commits = gather_commits(config)
    proj_info = extract_project_info(config, commits)
    draw_burn_up(config, commits, proj_info)
    draw_code_metrics(commits, proj_info)


if __name__ == "__main__":
    config = ProjectConfiguration(
        title="TODO burn-up",
        code_paths=("src", "."),
        tests_paths=("tests", "test", "."),
        language=PYTHON_CONFIGURATION,
    )
    draw_all(config)
